from app.models.orders_products_model import OrdersProducts
from flask import jsonify
from app.models.orders_model import Order


def list_all_orders():
    orders_list = Order.get_all()
    return jsonify(orders_list), 200


def get_orders_by_id(id: int):
    order = Order.get_by_id(id)

    order.itens = []

    for item in order.get_products():
        item = OrdersProducts(item).__dict__
        order.itens.append(item)

    return jsonify(order.__dict__), 200
