from flask import Flask, jsonify
from app.models.product_model import Product


def init_app(app: Flask):
    @app.get("/products")
    def list_all_products():
        products_list = Product.get_all()
        return jsonify(products_list), 200

    @app.get('/products/<int:id>')
    def get_product_by_id(id: int):
        product = Product.get_by_id(id)
        return product, 200
