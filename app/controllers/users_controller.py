from app.exceptions.user_exception import UserNotFound
from flask import request, jsonify
from app.models.users_model import User
from psycopg2.errors import UniqueViolation

    
def list_all_users():
    users_list = User.get_all()
    return jsonify(users_list), 200


def get_user_by_id(id: int):
    user = User.get_by_id(id)
    return user, 200


def delete_user_by_id(id: int):
    try:
        deleted_user = User.delete(id)
        return deleted_user, 200
    except UserNotFound as err:
        return {"message": str(err)}, 404
    

def update_user_by_id(id: int):
    try:
        data = request.get_json()
        updated_user = User.update(id, data)
        return updated_user, 200
    except UserNotFound as err:
        return {"message": str(err)}, 404


def create_user():
    try:
        data = request.get_json()
        user = User(data)
        new_user = user.save()
        return new_user, 201
    except UniqueViolation as err:
        return {"message": str(err).split("\n")[-2]}, 409
        
    
