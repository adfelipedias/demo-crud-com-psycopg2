from app.exceptions.invoices_exception import InvoiceNotFound
from flask import jsonify
from app.models.invoices_model import Invoice


def list_all_invoices():
    invoice_list = Invoice.get_all()
    return jsonify(invoice_list), 200


def get_invoice_by_id(id: int):
    try:
        invoice = Invoice.get_by_id(id)
        return invoice, 200
    except InvoiceNotFound as err:
        return {"message": str(err)}
