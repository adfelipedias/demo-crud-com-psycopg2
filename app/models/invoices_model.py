from app.exceptions.invoices_exception import InvoiceNotFound
import psycopg2
from typing import Union
from .configs import configs


def create_table():
    conn = psycopg2.connect(**configs)

    cur = conn.cursor()

    cur.execute(
        """
        CREATE TABLE IF NOT EXISTS invoices (
            invoice_id              SERIAL PRIMARY KEY, 
            invoice_number          varchar(255) UNIQUE,
            release_date            timestamp NOT NULL,
            order_id                int REFERENCES orders(order_id)
        );
        """
    )

    conn.commit()
    cur.close()
    conn.close()


class Invoice():
    def __init__(self, data: Union[tuple, dict]) -> None:
        if type(data) is tuple:
            self.invoice_id ,self.invoice_number, self.release_date, self.order_id = data
            
        elif type(data) is dict:
            for key, value in data.items():
                setattr(self, key, value)

    @staticmethod
    def get_all():
        conn = psycopg2.connect(**configs)
        cur = conn.cursor()

        cur.execute('SELECT * FROM invoices;')

        fetch_result = cur.fetchall()

        conn.commit()
        cur.close()
        conn.close()

        serialized_data = [Invoice(invoice_data).__dict__ for invoice_data in fetch_result]

        return serialized_data

    @staticmethod
    def get_by_id(id):
        
        conn = psycopg2.connect(**configs)
        cur = conn.cursor()
 
        cur.execute('SELECT * FROM invoices WHERE invoice_id=(%s);', (id, ))

        fetch_result = cur.fetchone()

        if not fetch_result:
            raise InvoiceNotFound(f'Nota Fiscal {id} não encontrada')

        conn.commit()
        cur.close()
        conn.close()

        invoice = Invoice(fetch_result).__dict__

        return invoice



