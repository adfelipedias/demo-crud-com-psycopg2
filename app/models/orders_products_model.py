import psycopg2
from typing import Union
from .configs import configs

def create_table():
    conn = psycopg2.connect(**configs)
    cur = conn.cursor()

    cur.execute(
        """
        CREATE TABLE IF NOT EXISTS orders_products (
            order_id           int NOT NULL REFERENCES orders(order_id)
            product_id         int NOT NULL REFERENCES products(product_id),
            value              real NOT NULL
        );
        """
    )

    conn.commit()
    cur.close()
    conn.close()


class OrdersProducts():
    def __init__(self, data: Union[tuple, dict]) -> None:
        if type(data) is tuple:
            self.product_name, self.value = data

        elif type(data) is dict:
            for key, value in data.items():
                setattr(self, key, value)
