# se importar qualquer coisa de models será executado
# o código que estiver aqui dentro

from . import users_model, product_model, invoices_model

users_model.create_table()
product_model.create_table()
invoices_model.create_table()