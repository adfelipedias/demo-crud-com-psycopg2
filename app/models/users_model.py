from app.exceptions.user_exception import UserNotFound
import psycopg2
from psycopg2 import sql
from typing import Union
from .configs import configs

# Isso tem que executar toda vez que a aplicação subir
# pois não tem como falar em tabela de usuário se não tiver a tabela
def create_table():
    conn = psycopg2.connect(**configs)

    cur = conn.cursor()

    cur.execute(
        """
        CREATE TABLE IF NOT EXISTS users (
            user_id              SERIAL PRIMARY KEY, 
            email                varchar(255) UNIQUE NOT NULL,
            birthdate            date NOT NULL,
            account_balance      real
        );
        """
    )

    conn.commit()
    cur.close()
    conn.close()


class User():
    def __init__(self, data: Union[tuple, dict]) -> None:
        if type(data) is tuple:
            self.user_id, self.email, self.birthdate, self.account_balance = data

        elif type(data) is dict:
            # items transforma um dicionário em uma lista de tuplas
            # setattr da valor para cada key
            for key, value in data.items():
                setattr(self, key, value)

    @staticmethod
    def get_all():
        conn = psycopg2.connect(**configs)
        cur = conn.cursor()

        cur.execute('SELECT * FROM users;')

        fetch_result = cur.fetchall()

        conn.commit()
        cur.close()
        conn.close()

        # para cada user_data será instanciado um novo usuário.
        serialized_data = [User(user_data).__dict__ for user_data in fetch_result]

        return serialized_data

    @staticmethod
    def get_by_id(id):
        
        conn = psycopg2.connect(**configs)
        cur = conn.cursor()

        # '%s' serve para passar uma variável para dentro do comando   
        cur.execute('SELECT * FROM users WHERE user_id=(%s);', (id, ))

        fetch_result = cur.fetchone()

        conn.commit()
        cur.close()
        conn.close()

        user = User(fetch_result).__dict__

        return user

    @staticmethod
    def delete(id):
        
        conn = psycopg2.connect(**configs)
        cur = conn.cursor()

        # '%s' serve para passar uma variável para dentro do comando   
        cur.execute('DELETE FROM users WHERE user_id=(%s) RETURNING *;', (id, ))

        fetch_result = cur.fetchone()

        conn.commit()
        cur.close()
        conn.close()

        if not fetch_result:
            raise UserNotFound(f"Usuário com id {id} não foi encontrado.")

        user = User(fetch_result).__dict__
        return user

    @staticmethod
    def update(id: int, data):

        conn = psycopg2.connect(**configs)
        cur = conn.cursor()

        columns = [sql.Identifier(key) for key in data.keys()]
        values = [sql.Literal(value) for value in data.values()]

        query = sql.SQL(
            """
                UPDATE
                    users
                SET
                    ({columns}) = row({values})
                WHERE
                    user_id={id}
                RETURNING *
            """).format(id=sql.Literal(str(id)),
                       columns=sql.SQL(',').join(columns),
                       values=sql.SQL(',').join(values))
        
        cur.execute(query)
        fetch_result = cur.fetchone()
      
        conn.commit()
        cur.close()
        conn.close()

        if not fetch_result:
            raise UserNotFound(f'Usuário com id {id} não encontrado.')
        
        serialized_data = User(fetch_result).__dict__

        return serialized_data

    def save(self):

        conn = psycopg2.connect(**configs)
        cur = conn.cursor()

        columns = [sql.Identifier(key) for key in self.__dict__.keys()]
        values = [sql.Literal(value) for value in self.__dict__.values()]

        query = sql.SQL(
            """
                INSERT INTO
                    users (id, {columns})
                VALUES
                    (DEFAULT, {values})
                RETURNING *
            """).format(columns=sql.SQL(',').join(columns),
                        values=sql.SQL(',').join(values))
        
        cur.execute(query)
        fetch_result = cur.fetchone()
      
        conn.commit()
        cur.close()
        conn.close()
        
        serialized_data = User(fetch_result).__dict__

        return serialized_data

