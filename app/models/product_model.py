import psycopg2
from typing import Union
from .configs import configs


def create_table():
    conn = psycopg2.connect(**configs)

    cur = conn.cursor()

    cur.execute(
        """
        CREATE TABLE IF NOT EXISTS products (
            product_id              SERIAL PRIMARY KEY, 
            name                    varchar(255) NOT NULL,
            description             varchar(1023),
            unit_value              float NOT NULL
        );
        """
    )

    conn.commit()
    cur.close()
    conn.close()


class Product():
    def __init__(self, data: Union[tuple, dict]) -> None:
        if type(data) is tuple:
            self.product_id, self.name, self.description, self.unit_value = data
            
        elif type(data) is dict:
            for key, value in data.items():
                setattr(self, key, value)

    @staticmethod
    def get_all():
        conn = psycopg2.connect(**configs)
        cur = conn.cursor()

        cur.execute('SELECT * FROM products;')

        fetch_result = cur.fetchall()

        conn.commit()
        cur.close()
        conn.close()

        serialized_data = [Product(product_data).__dict__ for product_data in fetch_result]

        return serialized_data

    @staticmethod
    def get_by_id(id):
        
        conn = psycopg2.connect(**configs)
        cur = conn.cursor()
 
        cur.execute('SELECT * FROM products WHERE product_id=(%s);', (id, ))

        fetch_result = cur.fetchone()

        conn.commit()
        cur.close()
        conn.close()

        user = Product(fetch_result).__dict__

        return user



