import psycopg2
from typing import Union
from .configs import configs

def create_table():
    conn = psycopg2.connect(**configs)
    cur = conn.cursor()

    cur.execute(
        """
        CREATE TABLE IF NOT EXISTS orders (
            order_id            SERIAL PRIMARY KEY, 
            customer_id         int NOT NULL REFERENCES users(user_id),
            order_id            timestamp NOT NULL
        );
        """
    )

    conn.commit()
    cur.close()
    conn.close()


class Order():
    def __init__(self, data: Union[tuple, dict]) -> None:
        if type(data) is tuple:
            self.order_id, self.custumer_id, self.order_date = data

        elif type(data) is dict:
            for key, value in data.items():
                setattr(self, key, value)

    @staticmethod
    def get_all():
        conn = psycopg2.connect(**configs)
        cur = conn.cursor()

        cur.execute('SELECT * FROM orders;')
        fetch_result = cur.fetchall()

        conn.commit()
        cur.close()
        conn.close()

        serialized_data = [Order(order_data).__dict__ for order_data in fetch_result]
        return serialized_data

    @staticmethod
    def get_by_id(id):
        conn = psycopg2.connect(**configs)
        cur = conn.cursor()
  
        cur.execute('SELECT * FROM orders WHERE order_id=(%s);', (id, ))
        fetch_result = cur.fetchone()

        conn.commit()
        cur.close()
        conn.close()

        order = Order(fetch_result)
        return order

    def get_products(self):
        conn = psycopg2.connect(**configs)
        cur = conn.cursor()

        query = """
            SELECT p.name, op.value
            FROM orders AS o
            JOIN orders_products AS op ON o.order_id = op.order_id
            JOIN products AS p ON p.product_id = op.product_id
            WHERE o.order_id = %s;
        """


        cur.execute(query, (self.order_id, ))
        fetch_result = cur.fetchall()

        conn.commit()
        cur.close()
        conn.close()

        return fetch_result
