# O link das views com controller

from flask import Blueprint
from app.controllers.invoices_controller import list_all_invoices, get_invoice_by_id

bp = Blueprint('invoices_bp', __name__, url_prefix='/invoices')

bp.get("")(list_all_invoices)
bp.get('<int:id>')(get_invoice_by_id)