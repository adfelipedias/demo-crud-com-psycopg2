# O link das views com controller

from flask import Blueprint
from app.controllers.users_controller import create_user, list_all_users, get_user_by_id, update_user_by_id, delete_user_by_id

bp = Blueprint('users_bp', __name__, url_prefix='/users')

bp.post("")(create_user)
bp.get("")(list_all_users)
bp.get('<int:id>')(get_user_by_id)
bp.patch('<int:id>')(update_user_by_id)
bp.delete('<int:id>')(delete_user_by_id)