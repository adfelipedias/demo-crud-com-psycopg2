from flask import Blueprint
from . import orders_blueprints, users_blueprints, invoices_blueprints

bp = Blueprint('api', __name__, url_prefix='/api')

bp.register_blueprint(orders_blueprints.bp)
bp.register_blueprint(users_blueprints.bp)
bp.register_blueprint(invoices_blueprints.bp)
