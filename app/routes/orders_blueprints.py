# O link das views com controller

from flask import Blueprint
from app.controllers.orders_controller import list_all_orders, get_orders_by_id

bp = Blueprint('orders_bp', __name__, url_prefix='/orders')

bp.get("")(list_all_orders)
bp.get('<int:id>')(get_orders_by_id)