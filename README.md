## Um CRUD, fazendo inserções de maneira dinâmica sem abrir mão da segurança dos dados.

## Tecnologias:
- Python
- Python Dotenv
- Flask
- PostegreSQL
- Psycopg2
- Padrão Factory
- Blueprints
